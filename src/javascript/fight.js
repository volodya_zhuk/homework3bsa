export function fight(firstFighter, secondFighter) {
    let isSomebodyAlive = true;
    while (isSomebodyAlive)
    {
        let firstDamage=getDamage(firstFighter,secondFighter);
        if( checkAlive(firstFighter, secondFighter) === false)
        {break;}
        let secondDamage=getDamage(secondFighter,firstFighter);
        if( checkAlive(firstFighter, secondFighter) === false)
        {break;}
    }
    return firstFighter.health>0 ?firstFighter:secondFighter;

}

export function getDamage(attacker, enemy) {

    const damage= getHitPower(attacker)-getBlockPower(enemy);
    if(damage>0) {
        enemy.health = enemy.health - damage;
        return damage;
    }else return 0;

}

export function getHitPower(fighter) {
    const criticalHitChance= Math.random()*(2-1)+1;
    return fighter.attack*criticalHitChance;
}

export function getBlockPower(fighter) {
    const dodgeChance=Math.random()*(2-1)+1;
    return fighter.defense*dodgeChance;

}

function checkAlive (fighter1,fighter2) {
        return !!(fighter1.health > 0 & fighter2.health > 0);
}