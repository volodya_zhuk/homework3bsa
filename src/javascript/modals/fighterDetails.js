import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

export function createFighterDetails(fighter) {
  const  name  = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
  const imageElement=createElement({tagName:'img',className:'fighter-image'});

  nameElement.innerText = 'Name: '+name.name +'\n Attack: '+name.attack+ '\n Defense: '+name.defense+'\n Health: '+name.health+'\n';
  imageElement.src=fighter.source;
  fighterDetails.append(nameElement,imageElement);

  return fighterDetails;
}
