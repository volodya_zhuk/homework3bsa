import {createFighterDetails} from "./fighterDetails";
import {showModal} from "./modal";

export  function showWinnerModal(fighter) {
  const title = 'Winner of the battle';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}