import { callApi } from '../helpers/apiHelper';
import {API_URL} from "../constants/apiURL";
import {createElement} from "../helpers/domHelper";

export async function getFighters() {
  try {
    const endpoint = 'fighters.json';
    const apiResult = await callApi(endpoint, 'GET');
    
    return apiResult;
  } catch (error) {
    throw error;
  }
}

export async function getFighterDetails(id) {
    return fetch(API_URL+'details/fighter/'+id+'.json')
      .then(response => {
         if (!response.ok) {
             throw new Error('Failed load data');
         }
         return response.json();
     })
     .then(file => {
         const fighter=JSON.parse(atob(file.content));
         console.log(fighter);
        return fighter;
     })
      .catch(error => {
          console.warn(error);
          const errorDetails = createElement({ tagName: 'div', className: 'error-msg' });
          errorDetails.innerText='Failed to load this fighter :(';
          console.log(errorDetails);
      }).finally();
}

